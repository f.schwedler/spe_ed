FROM rust:1.48

WORKDIR /usr/src/spe_ed
COPY . .

RUN cargo install --path .

CMD ["spe_ed"]
