#!/bin/sh

MINTED_FILTER=documentation_ressources/pandoc-minted/pandoc-minted.py
META_FILE=documentation_ressources/metadata.yml
OUT_NAME=Theoretische-Ausarbeitung


pandoc -f markdown-implicit_figures README.md --filter=$MINTED_FILTER --metadata-file=$META_FILE --toc -s -o $OUT_NAME.tex
xelatex -shell-escape $OUT_NAME.tex
xelatex -shell-escape $OUT_NAME.tex
xelatex -shell-escape $OUT_NAME.tex

rm $OUT_NAME.aux
rm $OUT_NAME.log
rm $OUT_NAME.toc
rm $OUT_NAME.tex

