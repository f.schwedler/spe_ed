# 1 Lösungsansatz

## 1.1 Ziel    
Das Ziel ist es, als letzter Spieler lebend auf dem Feld zu bleiben. 
Um dies zu gewährleisten, müssen folgende Kriterien erfüllt werden:

1. Keinen anderen Spieler berühren.
2. Keine vorhandene Spur berühren.
3. Nicht den Rand des Spielfeldes überschreiten. 
4. Nicht die erlaubte Geschwindigkeit unter- oder überschreiten.
5. Nicht die Zeit überschreiten, die gegeben wird, um den nächsten Zug zu generieren.  

Jeder Zug, der ein Kriterium verletzt, führt zum sofortigen Ausscheiden aus dem Spiel.

## 1.2 Strategie
Um das frühzeitige Ausscheiden aus dem Spiel zu verhindern, werden zuerst alle Züge, die eine der Kriterien von 1-4 verletzten, bei uns als illegal deklariert.
Daraufhin wird die Auswahl an möglichen Zügen neu berechnet und angepasst, sowie die nächsten Positionen berechnet, die mit dem Zug möglich wären. Hier werden erneut die Züge direkt aussortiert, die zum Ausscheiden aus dem Spiel führen würden.   

Um nun nicht einfach Runde für Runde nur irgendeinen oder immer denselben Zug zu machen, ohne dabei vorausschauend zu agieren, werden immer einige Iterationen im Voraus berechnet.
Die Anzahl an Iterationen ist dabei abhängig von der zulässigen Zeitspanne, in der unsere Antwort erwartet wird (siehe Kriterium 5).
Die berechneten Züge ergeben mehrere Varianten, in welche Richtung oder mit welcher Geschwindigkeit wir uns bewegen könnten. Dabei werden die Möglichkeiten Runde für Runde der Iteration aufaddiert.
Unsere Antwort wird dann zufällig unter den Varianten gewählt, die uns am längsten überleben lassen, die also eine der meisten Möglichkeiten hat.
Dabei wird jedoch nur die derzeitige Lage auf dem Spielfeld berücksichtigt und nicht die möglichen Züge der anderen Spieler.

# 2 Benutzerhandbuch

Dieser Abschnitt beschreibt alle wesentlichen Inhalte, um unsere Implementation richtig zu kompilieren und zu benutzen.

## 2.1 Abhängigkeiten
Für die erfolgreiche Kompilierung der Software wird eine Installation des Rust Compilers `rustc` und des Rust Buildsystems `cargo` benötigt.
Am einfachsten ist das über [rustup](https://rustup.rs) möglich.
Alle weiteren Abhängigkeiten werden dann von Cargo geholt und nach Bedarf kompiliert.

Wir nutzen folgende Bibliotheken:

- [*websocket*](https://crates.io/crates/websocket) um den Websocket zum Server auf machen zu können
- [*serde*](https://crates.io/crates/serde) für Serialisierung and Deserialisierung von [JSON](https://crates.io/crates/serde_json) (und [YAML](https://crates.io/crates/serde_yaml)) in, und von, Rust Datenstrukturen
- [*rand*](https://crates.io/crates/rand) für Zufallszahlen und zufälliges Auswählen aus einer Liste
- [*chrono*](https://crates.io/crates/chrono) für Angabe der Zeit und Zeitabstände
- [*clap*](https://crates.io/crates/clap) zum parsen der Kommandozeilenargumente
- [*hex*](https://crates.io/crates/hex) um Farben aus Hex Werten zu dekodieren
- [*log*](https://crates.io/crates/log) & [*simplelog*](https://crates.io/crates/simplelog) um einfach in verschiedenen Log-Leveln Ausgaben realisieren zu können

## 2.2 Kompilieren, Ausführen und Installieren 


### mit Docker 

Das mitgelieferte Dockerfile kann benutzt werden, um ein lauffähiges Docker-Image zu bauen und das Programm in einem Container auszuführen.
Cargo und der Rust Compiler werden von dem Dockerfile geholt. Es ist also außer Docker nichts weiteres notwendig.

#### Kompilieren/bauen des Dockerimages

Dazu muss im Terminal 

```
docker build --tag  griessgraemige_grauhaardackel .
```

eingegeben werden.

#### Ausführen
Nachdem das Docker-Image gebaut wurde, kann es gestartet werden.

```
docker run -e URL=<url> -e KEY=<api key> griessgraemige_grauhaardackel
```
Es kann auch noch eine Ausgabe mit `-e SPE_ED_LOG=<term|simple|file|debug>` eingeschaltet werden.


### mit Cargo
Das Programm kann auch mit dem Rust Buildsystem [Cargo](https://de.wikipedia.org/wiki/Cargo_(Software)) gebaut werden.

#### Kompilieren

```
cargo build --release
```

#### Ausführen
Mit dem Befehl

```
cargo run
```

... wird die Software kompiliert, falls das nötig ist, und direkt ausgeführt.

Es ist auch möglich direkt das Binary in `target/release/spe_ed` auszuführen.

Die Nötigen Kommandozeilenoptionen müssen natürlich noch ergänzt werden.
Wenn das Programm über Cargo gestartet wird müssen die Optionen von denen für Cargo durch ein `--` getrennt werden.

#### Installieren

Installieren läßt sich das Programm mit cargo über

```
cargo install --path .
```

... damit wird es im System installiert und läßt sich ab dann einfach über den Befehl `spe_ed` ausführen. Vorausgesetzt Cargo wurde vorher korrekt installiert und mit ausreichend Rechten zur Installation gestartet.



## 2.3 Benutzung der Software

Wenn die das Programm mittels Cargo gestartet wird `cargo run -- [FLAGS] [OPTIONS]` anstatt `spe_ed [FLAGS] [OPTIONS]`

```
USAGE:
    spe_ed [FLAGS] [OPTIONS]

FLAGS:
    -d               the Debug level
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <CONFIG>    configuration file with url and key (default ./config.yml)
                             [env: SPE_ED_CONF=] [default: ./config.yml]
    -k, --key <KEY>          Set the api key [env: KEY=]
    -l, --log <log>...       list of Loggers to use [env: SPE_ED_LOG=]
                             [possible values: simple, file, debug, term]
    -u, --url <URL>          The server to connect to [env: URL=]
```

Die Werte für *Key* und *url* können zusätzlich auch noch in einer Konfigurationsdatei Angegeben werden.

```yaml
url: "<url>"
key: "<key>"
```

# 3 Softwarearchitektur

## 3.1 Architektur
![Architektur Übersicht](documentation_ressources/Softwarearchitektur.png)

### Client
Die `Client` Klasse dient als unser zentraler Verbindungspunkt. Einerseits baut sie die Verbindung zum Server auf und gibt den neuen Status an die Strategie weiter, um den neuen Zug zu berechnen. Danach gibt sie die Antwort wiederum an den Server zurück.
Andererseits ruft `Client` die Methoden der `Log` Klassen auf, welche für die Ausgabe oder das Speichern in Logdateien zuständig sind.

### Server
Das vom Server kommende JSON wird mit Hilfe der Bibliothek [serde](https://crates.io/crates/serde) in Rust Klassen übersetzt, um sie für den Rest des Programms nutzbar machen zu können.
Genauso wird die von der Strategie bestimmte Aktion mittels serde zu JSON konvertiert um an den Server geschickt zu werden.

### Strategie
Für die Strategie haben wir ein abstraktes Interface implementiert. Jede Strategie-Klasse muss die Methode `turn` implementieren, welche für jede Runde den neuen Zustand des Feldes bekommt und eine `Action` zurückgibt, die den nächsten Zug mitteilt.

### Log Klassen
Um mitzubekommen was im Spiel passiert, ist es möglich verschiedene Log Module zu verwenden.
Um als Log Modul zu funktionieren, müssen diese Klassen das abstrakte Interface `GameLog` implementieren. Im Anschluss können sie dann zur Realisierung verschiedenster Ausgaben verwendet werden.

Das `GameLog` Interface besteht aus den zwei Methoden `field` und `action`.
Diese sind jeweils dazu da, den aktuellen Zustand des Feldes, wie wir ihn vom Server bekommen, auszugeben/zu loggen, oder die von der Strategie berechnete Aktion an den Server auszugeben/zu loggen.

#### Simple
`Simple` implementiert einen sehr simplen log Output, das jede Runde nur den aktuellen Zustand der Spieler mit Geschwindigkeit, Fahrtrichtung und aktueller Position ausgibt.

![eine Runde in Simple](documentation_ressources/simple-gamestate.png)

#### Debug
Diese Klasse gibt für jede Runde sowohl für das neue Feld, als auch für die Aktion den von Rust per `#[derive(Debug)]` generierten output aus.
Das bedeutet alle Member Variablen werden exakt wie sie sind ausgegeben.
Da das auch das gesamte Feld mitsamt aller Zellen umfasst ist diese Ausgabe nur sehr eingeschränkt nützlich.

Feld:

```
Field { width: 67, height: 76, cells: [[0, 0, ...],...], players: {2: Player { x: 6, y: 55, direction: Down, speed: 4, turn: 4, active: true, name: None }, 1: Player { x: 34, y: 35, direction: Left, speed: 1, turn: 7, active: true, name: None }, 3: Player { x: 22, y: 60, direction: Down, speed: 5, turn: 3, active: true, name: None }}, you: 1, running: true, deadline: 2021-01-16T16:12:34Z }
```

Antwort:

```
Reply { action: ChangeNothing }
```

#### SimpleTerminal

`SimpleTerminal` implementiert mit Unicode und ANSI eine farbige Terminal Ausgabe des Spielfeldes, sowie für den Zustand der einzelnen Spieler.
Für jeden aktiven Spieler wird die Fahrtrichtung, Geschwindigkeit und aktuelle Position ausgegeben. Für bereits ausgeschiedene Spieler wird die Nummer und der Name (falls bekannt) ausgegeben.

![Laufendes Spiel](documentation_ressources/laufendes-spiel.png)

![Beendetes Spiel](documentation_ressources/spiel-zu-ende.png)

#### FileLog
Schreibt den gesamten Spielverlauf, also den jeweiligen Zustand des Feldes und die darauf gespielten eigenen Züge, zusammen mit einem Zeitstempel als JSON in eine Datei.

### Beschreibung des Spielzustands

#### Player 
Die Klasse `Player` beschreibt den Zustand der Spieler, also ihre Position, Geschwindigkeit, etc.  Außerdem gibt es Methoden um alle legalen Züge und alle möglichen neuen Postitionen zu ermittelt.

#### Field
In `Field` wir der Zustand des Feldes beschrieben, es ist im Prinzip eine Eins zu Eins Übersetzung des vom Server bekommen JSON in eine Rust Klasse.
Zudem gibt es die Methode `recursive_positions_by_player`, welche Rekursiv, für eine gegebene Anzahl an Runden, alle Möglichen neuen Positionen bestimmt und die Summe an Möglichkeiten, sowie die erreichbare Anzahl an Runden, für jede dieser Positionen zurück gibt.
Sie Diese Methode bildet den Zentralen Teil unserer Strategie.
Einige, der dort beschrieben Methoden, werden nicht genutzt, da sie nicht mit der finalen Strategie einher gehen, ließen sich aber ohne Weites für neue Strategien benutzen.

#### Direction
`Direction` gibt eine Bewegungsrichtung an.
Zusätzlich gibt es Methoden, welche einen Vektor `(x, y)` relativ zur Richtung oder zum globalen Raum drehen. 

#### Reply 
Mit der Klasse Reply wird die Aktion, also der nächste Zug des Spielers angegeben.

### Datei Struktur

```
/
|-- Cargo.lock
|-- Cargo.toml
|-- Dockerfile
|-- example.config.yml
|-- README.md
`-- src/
    |-- main.rs
    |-- lib.rs
    |-- cli.rs
    |-- instance.rs
    |-- game/
    |   |-- mod.rs
    |   |-- direction.rs
    |   |-- distance_map.rs
    |   |-- field.rs
    |   |-- player.rs
    |   `-- reply.rs
    |-- log/
    |   |-- mod.rs
    |   |-- debug.rs
    |   |-- filelog.rs
    |   |-- simple.rs
    |   `-- simple_terminal.rs
    `-- strategie/
        |-- mod.rs
        |-- default.rs
        `-- random.rs
```
In der Datei `main.rs` ist die zentrale Datei für das Programm. Die Datei `lib.rs` fürt die Bibliotheken zusammen und ist damit ihr oberster Knotenpunkt.
`cli.rs` definiert die Kommandozeilenargumente und `instance.rs` beschreiben die Klasse `Client`.
Im Ordner `strategie/` liegen alle Strategien und in der Datei `mod.rs` wird das `Strategie`-Interface definiert.
Im Ordner `game/` liegen alle Klassen und Funktionen, die den Spielzustand beschreiben, z.B. player, field, direction, reply.
Dort liegt die Klasse DistanceMap in `distance_map.rs`, welche wir nicht genutzt haben, da sich unsere Strategieüberlegung geändert hat.

## 3.2 Software testing
Durch wiederholtes Ausführen der Software haben wir diese auf Probleme geprüft und die vom Compiler angezeigten Fehler und Warnungen behoben. 
Da Der Rust Kompiler über die Statische Datentypen bereits überprüft das immer passende Daten verwendet werden, hielten wir Unit- und Intergrationstests bei dem Umfang der Software nicht mehr für nötig, und haben unsere Zeit und Energie lieber auf die Funktionalität des Projektes verwendet.

## 3.3 Coding conventions
Die `Client` Klasse verwendet das Builder Pattern, wodurch es sich nach und nach mit verschiedenen `Log`/Ausgabe Varianten versorgen lässt.
Die einzelnen Klassen sind in einzelne Modul Dateien aufgeteilt, und über Interfaces verbunden, wodurch das Projekt übersichtlich und flexibel bleibt.

Zur Dokumentation des Quellcodes der Implementation verwenden wir Dokumentationskommentare im Quellcode an den jeweiligen Methoden, die mit `cargo doc` zu einer HTML Übersicht über alle Methoden generiert werden können, welches standardmäßig in Cargo integriert ist.
Die generierte Dokumentation kann dann mittels Webbrowser im Verzeichnis target/doc/spe_ed betrachtet werden. Der Befehl `cargo doc --open` öffnet die generierte Dokumentation automatisch im Webbrowser.

## 3.4 Wartbarkeit
Dadurch, dass die einzelnen Teile stark gekapselt und über abstrakte Interfaces verbunden sind, lässt sich problemlos jedes Modul ersetzen, ohne den Rest ändern oder auch nur anschauen zu müssen.
So ist es einfach das Programm um weitere Ausgabemöglichkeiten zu ergänzen, da lediglich ein Interface implementiert werden muss.

Folgendes Beispiel zeigt eine Implementierung der `GameLog`-Trait: 
```rust
use super::GameLog;

use crate::game::{
    Field,
    Reply,
};

pub struct Debug;

/// just log the debug output to standard out
impl GameLog for Debug {
    /// log the field to standard out
    fn field(&mut self, field: Field) {
        println!("{:?}", field);
    }
    /// log the move to standard out
    fn action(&mut self, reply: Reply) {
        println!("{:?}", reply);
    }
}
```
Im Beispiel wird ein Log implementiert, der den Debug-Output, der mittels `#[derive(Debug)]` von Rust automatisch generiert wurde, in der Konsole ausgibt.

Selbiges Prinzip gilt natürlich auch für eine neue Strategie, die genauso leicht hinzugefügt werden kann.

Eine Beispiel Implementierung für neuen Strategie wäre z.B. folgendes:
```rust
use spe_ed::{
	game::{
		Action,
		Field,
		Reply,
	},
	strategie::Strategie,
};
struct CircleStrategie {
	last: Action,
}

impl CircleStrategie {
	fn new() -> CircleStrategie {
		CircleStrategie { last: Action::SpeedUp }
	}
}
impl Strategie for CircleStrategie {
	/// Implementiert eine Strategie die,
	/// ohne das feld zu beachten versucht im Kreis zu fahren
	fn turn(&mut self, _state: Field) -> Reply {
		let action = match self.last {
			Action::SpeedUp => Action::TurnLeft,
			Action::TurnLeft => Action::SpeedUp,
			_ => Action::ChangeNothing,
		}
		self.last = action.clone();
		Reply::new(action)
	}
}
```
Hierbei wird eine Strategie implementiert, bei der der Spieler im Kreis um sich selber fährt, ohne dabei jedoch auf das Feld zu achten.

Unsere Software ist also sehr gut anpassbar bzw. wartbar und neue Strategien sind leicht zu implementieren. 


# 4 Diskussion

Um zu testen wie gut sich unsere Strategie schlägt, haben wir einige Spiele durchlaufen. Dabei haben wir häufig gewonnen aber auch einige Male verloren, sind jedoch nie auf dem letzten Platz gewesen.  
In den Spielen, in denen wir nicht gesiegt haben, sind wir entweder in Gegner hineingefahren, die zur selben Zeit auf dasselbe Feld wollten oder sind in Situationen gelangt, aus denen wir keinen Ausweg mehr hatten und damit gestorben sind.  
Es wurde aber auch klar, dass wir die Möglichkeit zu Springen oft und sinnvoll nutzen und der Blick in die Zukunft uns häufig zum Sieg verholfen hat. Zudem sorgt der Zufallsanteil unserer Strategie dafür, dass Gegner, dessen KI unsere Züge in ihre Überlegungen mit einbeziehen, dies nur schwer einschätzen können.  

Unsere Implementation könnte also verbessert werden, indem man noch mehr Iterationen voraus berechnet oder alle möglichen Züge der Gegner in die Berechnung des nächsten Zuges mit einbezieht. Ersteres war uns nicht möglich, da es die Antwortzeit extrem verlängert hat, weswegen wir uns entschieden haben, die Iterationsanzahl daran fest zu machen, wie viel Zeit wir für das Geben einer Antwort zu Verfügung haben.

