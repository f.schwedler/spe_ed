pub mod cli;
pub mod strategie;
pub mod log;
//pub mod field;
pub mod game;

mod instance;

pub use self::instance::Client;
