
mod default;
mod random;

// to use `spe_ed::strategie::{Default, Random};`
// instead of `spe_ed::strategie::{default::Default,random::Random};`
pub use self::{
    default::Default,
    random::Random,
};

#[allow(dead_code)]

use crate::game::{
    Field,
    Reply,
};

/// Example how you could implement a Strategie
/// that just Drives in a Circle
/// cant live very long because it dosn't consider
/// if it would go faster than 10
/// ```
/// use spe_ed::{
/// 	game::{
/// 		Action,
/// 		Field,
/// 		Reply,
/// 	},
/// 	strategie::Strategie,
/// };
/// struct CircleStrategie {
/// 	last: Action,
/// }
/// impl CircleStrategie {
/// 	fn new() -> CircleStrategie {
/// 		CircleStrategie { last: Action::SpeedUp }
/// 	}
/// }
/// impl Strategie for CircleStrategie {
/// 	/// Implementiert eine Strategie die,
/// 	/// ohne das feld zu beachten versucht im Kreis zu fahren
/// 	fn turn(&mut self, _state: Field) -> Reply {
/// 		let action = match self.last {
/// 			Action::SpeedUp => Action::TurnLeft,
/// 			Action::TurnLeft => Action::SpeedUp,
/// 			_ => Action::ChangeNothing,
/// 		};
/// 		self.last = action.clone();
/// 		Reply::new(action)
/// 	}
/// }
/// ```
pub trait Strategie {
    /// Calculate the next move from the current state
    fn turn(&mut self, _state: Field) -> Reply {
        Reply::default()
    }
}
