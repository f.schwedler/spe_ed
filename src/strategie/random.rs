
use crate::strategie::Strategie;
use crate::game::{
    Reply,
    Action,
    Field,
};
use log::info;

use rand::seq::SliceRandom;

#[derive(Debug)]
pub struct Random {
    last: Action,
}

impl Random  {
    pub fn new() -> Random {
        Random { last: Action::default() }
    }
}

impl Strategie for Random {
    /// just do a random leagal move every time
    /// never repeating the last move
    fn turn(&mut self, state: Field) -> Reply {
        //let alowed = state.undead_actions_for_player(state.you());
        let alowed = state.recursive_positions_by_player(&state.you(), 8);
        //println!("{:?}", alowed);
        //let new = match alowed.choose(&mut rand::thread_rng()) {
        let new_pos = match alowed.choose_weighted(&mut rand::thread_rng(), |item| item.1) {
            Ok((p,_,_)) => p,
            _ => {
                //return Reply::new(Action::default()),
                match alowed.choose_weighted(&mut rand::thread_rng(), |item| item.2) {
                    Ok((p,_,_)) => p,
                    _ => return Reply::new(Action::default()),
                }
            }
        };
        let ac = state.you().action_from_position(new_pos);
        info!("{{{} {} {:?}}} -> {{{} {} {:?}}} -> {:?} => {:?}",
            state.you().speed(), state.you().direction(), state.you().pos(),
            new_pos.speed(), new_pos.direction(), new_pos.pos(),
            state.you().direction().positions_to_vector(state.you().pos(), new_pos.pos()),
            ac);
        let new = match ac {
            Some(a) => a,
            _ => return Reply::new(Action::default()),
        };
        Reply::new(new)
    }
}
