
use crate::strategie::Strategie;

use crate::game::{
    Field,
    Reply,
};

#[derive(Debug)]
pub struct Default;

impl Strategie for Default {
    /// never changed anything just go forward at a speed of 1 🤷
    fn turn(&mut self, _state: Field) -> Reply {
        Reply::default()
    }
}
