
use crate::strategie::Strategie;
use crate::game::Field;
use crate::log::GameLog;
use std::collections::HashMap;
use log::{
    debug,
    error,
};
use websocket::{
    client::ClientBuilder,
    OwnedMessage
};
use chrono::Utc;

//#[derive(Debug)]
pub struct Client<T: Strategie> {
    url: String,
    key: String,
    strategie: T,
    loggers: Vec<Box<dyn GameLog>>,
}

impl<T> Client<T> where T: Strategie {
    /// create a new Client
    pub fn new(url: String, key: String, strat: T) -> Self {
        Client { url: url, key: key, strategie: strat, loggers: Vec::new() }
    }
    /// add a Logger to the Client, can be called multiple times
    pub fn log<L: 'static + GameLog>(&mut self, logger: L) -> &Client<T> {
        let l = Box::new(logger);
        self.loggers.push(l);
        self
    }
    /// adds a list of Loggers to the Client
    pub fn logger(&mut self, loggers: Vec<Box<dyn GameLog>>) -> &Client<T> {
        let mut loggers = loggers;
        self.loggers.append(&mut loggers);
        self
    }
    /// start the game
    pub fn run(&mut self) {
        let con = format!("{}?key={}", self.url, self.key);
        // Connect to server
        let mut connection = match ClientBuilder::new(&con).unwrap().connect(None) {
            Ok(c) => c,
            Err(e) => panic!("Connection Failed: {:?}", e),
        };
        let mut last_round = HashMap::new();
        loop {
            // get the playing field/new state of the game
            let mut new_state: Field = match connection.recv_message() {
                Ok(OwnedMessage::Text(json)) => {
                    match serde_json::from_str(&json) {
                        Ok(f) => f,
                        Err(e) => {
                            error!("Parse Error: {:?}", e);
                            error!("{}", json);
                            break;
                        }
                    }
                }
                _ => {
                    //eprintln!("End {:?}", e);
                    break; // end when no readable message was found
                }
            };
            let round_start_time = Utc::now();
            let deadline = new_state.deadline();
            last_round = new_state.set_player_turns(&last_round);
            // give the state to the loggers
            for log in &mut self.loggers {
                log.field(new_state.clone());
            }
            // skip the rest if the player is no longer active
            if !new_state.you().active() {
                continue;
            }
            // get the next move from strategie
            let next_move = self.strategie.turn(new_state);
            // send the move
            let message = OwnedMessage::Text(serde_json::to_string(&next_move).unwrap());
            match connection.send_message(&message) {
                Ok(()) => (),
                Err(e) => {
                    error!("Send Error: {:?}", e);
                    error!("{:?}", message);
                }
            }
            let round_done_time = Utc::now();
            // log the move
            for log in &mut self.loggers {
                log.action(next_move.clone());
            }
            debug!("took: {}, left: {}", round_done_time.signed_duration_since(round_start_time), deadline.signed_duration_since(round_done_time));
        }
        // close the connection
        let _ = connection.send_message(&OwnedMessage::Close(None));
    }
}
