
use simplelog::{
    TermLogger,
    LevelFilter,
    TerminalMode,
};
use spe_ed::{
    cli,
    Client,
    strategie::Random,
    log,
};

fn main() {
    let arguments = cli::CliBuilder::new()
        .log("simple", log::Simple)
        .log("term", log::SimpleTerminal::new())
        .log("debug", log::Debug)
        .log("file", log::FileLog::new("log/"))
        .build();

    let log_config = simplelog::ConfigBuilder::new()
        .set_time_level(LevelFilter::Debug)
        .add_filter_allow_str("spe_ed")
        .build();
    let _ = TermLogger::init(arguments.debug_level(), log_config, TerminalMode::Mixed);

    let mut clien = Client::new(arguments.url(), arguments.key(), Random::new());
    clien.logger(arguments.loggers());

    println!("Starting Game!");

    // run a game
    clien.run();
    println!("done");
}
