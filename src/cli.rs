
use clap::{
    App,
    Arg,
    crate_authors,
    crate_description,
    crate_name,
    crate_version,
};
use std::{
    collections::HashMap,
    fs::File,
};
use crate::log::GameLog;
use simplelog::LevelFilter;

pub struct CliBuilder {
    posible_logger: HashMap<&'static str, Box<dyn GameLog>>,
    config: &'static str,
}

impl CliBuilder {
    pub fn new() -> CliBuilder {
        CliBuilder {
            config: "./config.yml",
            posible_logger: HashMap::new(),
        }
    }
    pub fn with_default_config(conf: &'static str) -> CliBuilder {
        CliBuilder {
            config: conf,
            posible_logger: HashMap::new(),
        }
    }
    pub fn log<L: 'static + GameLog>(mut self, name: &'static str, logger: L) -> Self {
        let l = Box::new(logger);
        self.posible_logger.insert(name, l);
        self
    }
    pub fn build(mut self) -> Cli {
        let arguments = self.build_cli().get_matches();

        // get the config file
        let config: HashMap<String, String> = match arguments.value_of("config") {
            Some(c) => {
                match File::open(c) {
                    Ok(f) => {
                        match serde_yaml::from_reader(f) {
                            Ok(conf) => conf,
                            Err(_) => HashMap::new(),
                        }
                    }
                    Err(_) => HashMap::new(),
                }
            }
            None => HashMap::new(),
        };
        // get the url, first from commandline arguemnt,
        // then enviroment variables and last from config file
        let url = match arguments.value_of("url") {
            Some(u) => u,
            None => {
                match config.get("url") {
                    Some(u) => u,
                    None => panic!("No URL to connect to")
                }
            }
        };
        // get the api key, first from commandline arguemnt,
        // then enviroment variables and last from config file
        let key = match arguments.value_of("key") {
            Some(k) => k,
            None => {
                match config.get("key") {
                    Some(k) => k,
                    None => panic!("No Key provided for connection")
                }
            }
        };

        let log_level = match arguments.occurrences_of("verbosity") {
            0 => LevelFilter::Off,
            1 => LevelFilter::Error,
            2 => LevelFilter::Warn,
            3 => LevelFilter::Info,
            4 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        };

        // get all the logger
        let mut loggers: Vec<Box<dyn GameLog>> = Vec::new();
        match arguments.values_of("log") {
            Some(values) => {
                for l in values {
                    match self.posible_logger.remove(l) {
                        Some(v) => loggers.push(v),
                        _ => ()
                    }
                }
            }
            _ => (),
        }
        Cli {
            key: key.to_owned(),
            url: url.to_owned(),
            debug_level: log_level,
            loggers: loggers,
        }
    }
    // Parser for the commandline Arguments
    fn build_cli(&self) -> App<'static> {
        let logger_names: Vec<&'static str> = self.posible_logger.keys().map(|k| *k).collect();
        App::new(crate_name!())
            .version(crate_version!())
            .author(crate_authors!("\n"))
            .about(crate_description!())
            .arg(Arg::new("url")
                .short('u')
                .long("url")
                .value_name("URL")
                .env("URL")
                .about("The server to connect to")
                .takes_value(true))
            .arg(Arg::new("key")
                .short('k')
                .long("key")
                .value_name("KEY")
                .env("KEY")
                .about("Set the api key")
                .takes_value(true))
            .arg(Arg::new("config")
                .short('c')
                .long("config")
                .aliases(&["conf", "configuration"])
                .value_name("CONFIG")
                .env("SPE_ED_CONF")
                .default_value(self.config)
                .about("configuration file with url and key (default ./config.yml)")
                .takes_value(true))
            .arg(Arg::new("log")
                .short('l')
                .long("log")
                .multiple(true)
                .env("SPE_ED_LOG")
                .about("list of Loggers to use")
                .takes_value(true)
                .possible_values(&logger_names[..])
                .multiple_values(true))
            .arg(Arg::new("debuglevel")
                .short('d')
                .about("the Debug level")
                .multiple_occurrences(true))
    }
}

pub struct Cli {
    key: String,
    url: String,
    debug_level: LevelFilter,
    loggers: Vec<Box<dyn GameLog>>,
}

impl Cli {
    pub fn key(&self) -> String {
        self.key.clone()
    }
    pub fn url(&self) -> String {
        self.url.clone()
    }
    pub fn debug_level(&self) -> LevelFilter {
        self.debug_level
    }
    pub fn loggers(mut self) -> Vec<Box<dyn GameLog>> {
        let loggers = self.loggers;
        self.loggers = Vec::new();
        loggers
    }
}
