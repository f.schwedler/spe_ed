
use super::GameLog;

use crate::game::{
    Field,
    Reply,
};

pub struct Debug;

/// just log the debug output to standard out
impl GameLog for Debug {
    /// log the field to standard out
    fn field(&mut self, field: Field) {
        println!("{:?}", field);
    }
    /// log the move to standard out
    fn action(&mut self, reply: Reply) {
        println!("{:?}", reply);
    }
}
