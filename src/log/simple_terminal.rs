
use super::{
    GameLog,
};
use crate::game::{
    Field,
    Reply,
};
use hex;


#[derive(Copy, Clone)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8
}

impl Color {
    pub fn color_map(index: i8) -> Self {
        let colors = vec![
            Color::black(),
            Color::from_hex("7900ff"),
            Color::from_hex("eb6f00"),
            Color::from_hex("00d867"),
            Color::from_hex("ff00c8"),
            Color::from_hex("c8ff00"),
            Color::from_hex("00c8ff"),
        ];
        if index < 0 || index >= colors.len() as i8 {
            Color::white()
        } else {
            colors[index as usize]
        }
    }
    fn black() -> Self {
        Color { red: 0, green: 0, blue: 0 }
    }
    fn white() -> Self {
        Color { red: 255, green: 255, blue: 255 }
    }
    fn from_hex(hex: &str) -> Self {
        let vals = hex::decode(hex).unwrap();
        Color { red: vals[0], green: vals[1], blue: vals[2] }
    }
}

pub struct SimpleTerminal {
    round: usize,
}

impl SimpleTerminal {
    pub fn new() -> SimpleTerminal {
        SimpleTerminal { round: 0 }
    }
    fn print_field(field: &Field) {
        for y in (0..field.height()).step_by(2) {
            for x in 0..field.width() {
                let upper_color = Color::color_map(field[(x, y)]);
                if y + 1 < field.height() {
                    let lower_color = Color::color_map(field[(x, y + 1)]);
                    print!("\x1b[38;2;{};{};{};48;2;{};{};{}m▀\x1b[0m",
                        upper_color.red, upper_color.green, upper_color.blue,
                        lower_color.red, lower_color.green, lower_color.blue);
                } else {
                    print!("\x1b[38;2;{};{};{};48;2;0;0;0m▀\x1b[0m",
                        upper_color.red, upper_color.green, upper_color.blue);
                }
            }
            println!("");
        }
    }
    fn print_players(&self, field: &Field) {
        for i in 1..field.players().len() + 1{
            let i = i as i8;
            let color = Color::color_map(i);
            print!("\x1b[30;48;2;{};{};{}m|", color.red, color.green, color.blue);
            if field.your_id() == i {
                print!("\x1b[1;3;4m");
            }
            print!("{}", i);
            let player = &field.players()[&i];
            if player.active() {
                print!(": {} {}, ({}, {})",
                player.direction(), player.speed(), player.x(), player.y());
            }
            match player.name() {
                Some(name) => print!(": {}", name),
                None => (),
            }
            print!("\x1b[0;30;48;2;{};{};{}m|\x1b[0m", color.red, color.green, color.blue);
        }
        println!(" Round: {}", self.round);
    }
}

impl GameLog for SimpleTerminal {
    fn field(&mut self, field: Field) {
        self.round += 1;
        SimpleTerminal::print_field(&field);
        self.print_players(&field);
    }
    fn action(&mut self, _reply: Reply) {
    }
}
