
use std::{
    path::Path,
    fs::{
        File,
        create_dir_all,
    }
};
use std::io::prelude::*;
use chrono::{
    prelude::DateTime,
    offset::Utc,
    SecondsFormat,
};
use super::GameLog;
use crate::game::{
    Action,
    Field,
    Reply,
};
use serde::{
    Serialize,
    Deserialize,
};

#[derive(Serialize, Deserialize, Debug)]
enum Data {
    Action(Action),
    Field(Field),
    Start,
}

#[derive(Serialize, Deserialize, Debug)]
struct LogData {
    time: DateTime<Utc>,
    data: Data,
}

pub struct FileLog {
    file: File,
}

impl FileLog {
    /// create the a file for the curent time
    /// in the given log directory,
    /// and write the Timestamp
    pub fn new(path: &str) -> FileLog {
        let path = match path {
            "" => ".",
            o => o,
        };
        let path = format!("{}/{}.json", path, Utc::now().to_rfc3339_opts(SecondsFormat::Secs, false));
        let path = Path::new(&path);
        match path.parent() {
            Some(p) => { let _ = create_dir_all(p); }
            _ => (),
        }
        let mut f = File::create(path).unwrap();
        let _ = f.write_all(b"[\n");
        let _ = serde_json::to_writer(&f, &LogData { time: Utc::now(), data: Data::Start });
        FileLog { file: f }
    }
}

// destuctor
impl Drop for FileLog {
    /// close the JSON Array
    fn drop(&mut self) {
        let _ = self.file.write_all(b"\n]");
    }
}

impl GameLog for FileLog {
    /// write the time and field state to the JSON file
    fn field(&mut self, field: Field) {
        let l = LogData {
            time: Utc::now(),
            data: Data::Field(field),
        };
        let _ = self.file.write_all(b",\n");
        let _ = serde_json::to_writer(&self.file, &l);
    }
    fn action(&mut self, reply: Reply) {
        let l = LogData {
            time: Utc::now(),
            data: Data::Action(reply.action),
        };
        let _ = self.file.write_all(b",\n");
        let _ = serde_json::to_writer(&self.file, &l);
    }
}
