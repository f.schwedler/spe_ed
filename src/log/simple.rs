
use super::GameLog;

use chrono::{
    //prelude::DateTime,
    offset::Utc,
};
use crate::game::{
    Field,
    Reply,
    Direction,
};

pub struct Simple;

fn direction_symbol(direction: Direction) -> String {
    match direction {
        Direction::Up => format!("🠝"),
        Direction::Down => format!("🠟"),
        Direction::Left => format!("🠜"),
        Direction::Right => format!("🠞"),
    }
}

impl GameLog for Simple {
    /// log the current Time, the deadline for the turn, the count of active players
    /// and for all player: the id, direction and speed,
    /// if there no longer active just the id and the name, if one is given.
    /// ```sh
    /// 2020-12-20 02:15:16.898315133 UTC - 2020-12-20 02:15:29 UTC |2||Me(4): 🠟 speed: 1||5: 🠟 speed: 4||1||3: 🠞 speed: 1| active: 3/5
    /// ```
    fn field(&mut self, field: Field) {
        let mut active = 0;
        print!("{} - {} ", Utc::now(), field.deadline());
        for (id, player) in field.players() {
            if field.your_id() == *id {
                print!("|Me({})", id);
            } else {
                print!("|{}", id);
            }
            if player.active() {
                active += 1;
                print!(": {} speed: {}", direction_symbol(player.direction()), player.speed());
            }
            match player.name() {
                Some(name) => print!(": {}|", name),
                None => print!("|"),
            }
        }
        println!(" active: {}/{}", active, field.players().len());
    }
    /// do's nothing, the Action taken is not printed
    fn action(&mut self, _reply: Reply) {
    }
}
