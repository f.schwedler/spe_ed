
mod simple;
mod debug;
mod filelog;
mod simple_terminal;

pub use self::{
    simple::Simple,
    debug::Debug,
    filelog::FileLog,
    simple_terminal::{
        SimpleTerminal,
        Color,
    },
};

use crate::game::{
    Field,
    Reply,
};

pub trait GameLog {
    /// log the new game state
    fn field(&mut self, field: Field);
    /// log the turn that was taken
    fn action(&mut self, reply: Reply);
}
