
use std::{
    collections::BinaryHeap,
    cmp::{
        Ordering,
        min,
        max,
    },
    ops::{
        Index,
        IndexMut,
    },
};
use crate::game::{
    Field,
    Player,
};

/// To save Player State/Position and number of turns taken for Dijkstra
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Position {
    player: Player,
    distance: Distance,
}

impl Ord for Position {
    fn cmp(&self, other: &Self) -> Ordering {
        other.distance.cmp(&self.distance).then_with(|| {
            self.player.cmp(&other.player)
        })
    }
}
impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Position {
    fn from_player(p: &Player) -> Position {
        Position {
            player: p.clone(),
            distance: Distance::Explicit(0),
        }
    }
    fn update_player(&self, field: &Field) -> Vec<Position> {
        let mut res = Vec::new();
        for p in field.next_positions_by_player(&self.player) {
            res.push(Position { player: p.clone(), distance: self.distance.plus_one() })
        }
        res
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Distance {
    Blocked,                      // Already blocked/not reachable
    NotReached,                   // distance unknown/not jet calculated
    Explicit(usize),              // Player could stand hear in that amount of turns
    Implicit(usize),              // Player could go over here in that amount of turns
    ExpImplicit(usize, usize),    // Value for Explicit and Implicit
}

impl Distance {
    fn plus_one(&self) -> Distance {
        match self {
            Distance::Blocked => Distance::Blocked,
            Distance::NotReached => Distance::NotReached,
            Distance::Explicit(dist) => Distance::Explicit(dist + 1),
            Distance::Implicit(dist) => Distance::Implicit(dist + 1),
            Distance::ExpImplicit(edist, idist) => Distance::ExpImplicit(edist + 1, *idist),
        }
    }
    fn update(&mut self, new: &Distance) {
        *self = match self {
            Distance::Blocked => new.clone(),
            Distance::NotReached => new.clone(),
            Distance::Explicit(dist) => match new {
                Distance::Blocked => Distance::Blocked,
                Distance::NotReached => Distance::NotReached,
                Distance::Explicit(ndist) => Distance::Explicit(*ndist),
                Distance::Implicit(ndist) => Distance::ExpImplicit(*dist, *ndist),
                Distance::ExpImplicit(nedist, nidist) => Distance::ExpImplicit(*nedist, *nidist),
            },
            Distance::Implicit(dist) => match new {
                Distance::Blocked => Distance::Blocked,
                Distance::NotReached => Distance::NotReached,
                Distance::Implicit(ndist) => Distance::Implicit(*ndist),
                Distance::Explicit(ndist) => Distance::ExpImplicit(*ndist, *dist),
                Distance::ExpImplicit(nedist, nidist) => Distance::ExpImplicit(*nedist, *nidist),
            },
            Distance::ExpImplicit(edist, idist) => match new {
                Distance::Blocked => Distance::Blocked,
                Distance::NotReached => Distance::NotReached,
                Distance::Implicit(ndist) => Distance::ExpImplicit(*edist, *ndist),
                Distance::Explicit(ndist) => Distance::ExpImplicit(*ndist, *idist),
                Distance::ExpImplicit(nedist, nidist) => Distance::ExpImplicit(*nedist, *nidist),
            }
        }
    }
    fn to_implicit(&self) -> Distance {
        match self {
            Distance::Blocked => Distance::Blocked,
            Distance::NotReached => Distance::NotReached,
            Distance::Implicit(dist) => Distance::Implicit(*dist),
            Distance::Explicit(dist) => Distance::Implicit(*dist),
            Distance::ExpImplicit(_, dist) => Distance::Implicit(*dist),
        }
    }
}

impl Ord for Distance {
    fn cmp(&self, other: &Self) -> Ordering {
        match self {
            Distance::Blocked => {
                match other {
                    Distance::Blocked => Ordering::Equal,
                    _ => Ordering::Less,
                }
            }
            Distance::NotReached => {
                match other {
                    Distance::NotReached => Ordering::Equal,
                    _ => Ordering::Greater,
                }
            }
            Distance::Explicit(dist) => {
                match other {
                    Distance::Blocked => Ordering::Greater,
                    Distance::NotReached => Ordering::Less,
                    Distance::Explicit(other_dist) => dist.cmp(other_dist),
                    Distance::Implicit(other_dist) => {
                        let c = dist.cmp(other_dist);
                        if Ordering::Equal == c {
                            Ordering::Less
                        } else {
                            c
                        }
                    }
                    Distance::ExpImplicit(other_edist, other_idist) => {
                        let c = dist.cmp(min(other_edist, other_idist));
                        if Ordering::Equal == c {
                            let d = dist.cmp(max(other_edist, other_idist));
                            if Ordering::Equal == d {
                                Ordering::Less
                            } else {
                                d
                            }
                        } else {
                            c
                        }
                    }
                }
            }
            Distance::Implicit(dist) => {
                match other {
                    Distance::Blocked => Ordering::Greater,
                    Distance::NotReached => Ordering::Less,
                    Distance::Explicit(other_dist) => {
                        let c = dist.cmp(other_dist);
                        if Ordering::Equal == c {
                            Ordering::Greater
                        } else {
                            c
                        }
                    }
                    Distance::Implicit(other_dist) => dist.cmp(other_dist),
                    Distance::ExpImplicit(other_edist, other_idist) => {
                        let c = dist.cmp(min(other_edist,other_idist));
                        if Ordering::Equal == c {
                            let d = dist.cmp(max(other_edist, other_idist));
                            if Ordering::Equal == d {
                                Ordering::Less
                            } else {
                                d
                            }
                        } else {
                            c
                        }
                    }
                }
            }
            expimp => other.cmp(expimp).reverse(),
        }
    }
}

impl PartialOrd for Distance {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Default for Distance {
    fn default() -> Distance {
        Distance::NotReached
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DistanceField {
    distance: Distance,
    player: Option<Player>,
}

impl DistanceField {
    pub fn new(dist: &Distance, player: &Player) -> DistanceField {
        DistanceField {
            distance: dist.clone(),
            player: Some(player.clone()),
        }
    }
    pub fn from_distance(dist: &Distance) -> DistanceField {
        DistanceField {
            distance: dist.clone(),
            player: None,
        }
    }
    pub fn from_position(pos: &Position) -> DistanceField {
        DistanceField {
            distance: pos.distance.clone(),
            player: Some(pos.player.clone()),
        }
    }
    pub fn update(&mut self, dist: &Distance, pos: &Player) {
        self.distance.update(dist);
        self.player = Some(pos.clone());
    }
    pub fn distance(&self) -> Distance {
        self.distance
    }
}

impl Ord for DistanceField {
    fn cmp(&self, other: &Self) -> Ordering {
        self.distance.cmp(&other.distance)
    }
}
impl PartialOrd for DistanceField {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Default for DistanceField {
    fn default() -> DistanceField {
        DistanceField {
            distance: Distance::default(),
            player: None,
        }
    }
}

#[derive(Debug)]
pub struct DistanceMap {
    cells: Vec<Vec<DistanceField>>,
}

impl DistanceMap {
    /// get the height of the `DistanceMap`
    pub fn height(&self) -> usize {
        self.cells.len()
    }
    /// get the with of the `DistanceMap`
    pub fn width(&self) -> usize {
        if 0 < self.cells.len() {
            self.cells[0].len()
        } else {
            0
        }
    }
    /// Builds a DistanceMap with the given Size,
    /// and all Fields as `Distance::NotReached`
    fn empty(x: usize, y: usize) -> DistanceMap {
        DistanceMap {
            cells: (0..y).map( |_| {
                (0..x).map(|_| DistanceField::default()).collect()
            }).collect()
        }
    }
    /// Builds a DistanceMap the same Size as the Field
    /// and every cell that isn't empty as `Distance::Blocked`,
    /// the rest is `Distance::NotReached`
    pub fn blocked_fields(field: &Field) -> DistanceMap {
        let mut map = DistanceMap::empty(field.width(), field.height());
        for y in 0..field.height() {
            for x in 0..field.width() {
                if 0 != field[(x, y)] {
                    map[(x,y)] = DistanceField::new(&Distance::Blocked, &field.players()[&field[(x,y)]]);
                }
            }
        }
        map
    }
    /// Build a `DistanceMap` For a `Player` by its id
    pub fn map_by_player_id(field: &Field, pid: &i8) -> DistanceMap {
        match field.players().get(pid) {
            Some(player) => DistanceMap::map_by_player(field, &player),
            None => DistanceMap::blocked_fields(field),
        }
    }
    /// Build a `DistanceMap` For a `Player`
    pub fn map_by_player(field: &Field, player: &Player) -> DistanceMap {
        let mut dist = DistanceMap::blocked_fields(field);

        if !player.active() {
            return dist;
        }

        let mut heap = BinaryHeap::new();

        let start = Position::from_player(player);

        // We're at `start`, with a zero cost
        dist[start.player.pos()] = DistanceField::new(&start.distance, &start.player);
        heap.push(start);

        // Examine the frontier with lower cost nodes first (min-heap)
        while let Some(position) = heap.pop() {
            // Alternatively we could have continued to find all shortest paths
            //if position == goal { return Some(cost); }

            // Important as we may have already found a better way
            if position.distance > dist[position.player.pos()].distance
                || dist[position.player.pos()].distance == Distance::Blocked { continue; }

            // For each node we can reach, see if we can find a way with
            // a lower cost going through this node
            for next in position.update_player(&field) {
                //let next = State { cost: cost + edge.cost, position: edge.node };
                let mut cont = false;
                // check the skiped fields
                for s in &next.player.skipped_fields() {
                    if Distance::Blocked != dist[*s].distance {
                        let imp = next.distance.to_implicit();
                        if dist[*s].distance > imp {
                            dist[*s].update(&imp, &next.player);
                        }
                    } else {
                        cont = true;
                        if Distance::Blocked != dist[next.player.pos()].distance
                            && dist[next.player.pos()].distance > next.distance {
                            dist[next.player.pos()].update(&next.distance.to_implicit(), &next.player);
                        }
                    }
                }
                if cont { continue; }
                // If so, add it to the frontier and continue
                if next.distance < dist[next.player.pos()].distance {
                    heap.push(next.clone());
                    // Relaxation, we have now found a better way
                    dist[next.player.pos()].update(&next.distance, &next.player);
                }
            }
        }
        dist
    }
    /// Builds a Map that shows wich player is nearest to each field
    pub fn joined_map(field: &Field) -> JoinedMap {
        let mut jmap = JoinedMap::empty(field.width(), field.height());
        let mut maps: Vec<Vec<DistanceField>> = Vec::new();
        for y in 0..field.height() {
            let mut lmap = Vec::new();
            for x in 0..field.width() {
                if 0 != field[(x, y)] {
                    let player = field.players()[&field[(x,y)]].clone();
                    lmap.push(DistanceField::new(&Distance::Blocked, &player));
                    jmap[(x,y)] = JoinedCell::from_id(field[(x,y)]);
                } else {
                    lmap.push(DistanceField::from_distance(&Distance::NotReached));
                }
            }
            maps.push(lmap);
        }
        for (i, p) in field.players() {
            let map = DistanceMap::map_by_player(&field, &p);
            for y in 0..field.height() {
                for x in 0..field.width() {
                    if map[(x, y)] < maps[y][x] {
                        maps[y][x] = map[(x,y)].clone();
                        jmap[(x, y)] = JoinedCell::with_option(i.clone(), map[(x,y)].player.clone());
                    }
                }
            }
        }
        jmap
    }
}

impl Index<(usize, usize)> for DistanceMap {
    type Output = DistanceField;
    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        &self.cells[y][x]
    }
}

impl IndexMut<(usize, usize)> for DistanceMap {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        &mut self.cells[y][x]
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct JoinedCell {
    player_id: i8,
    player: Option<Player>,
}

impl JoinedCell {
    pub fn new(pid: i8, player: &Player) -> JoinedCell {
        JoinedCell {
            player_id: pid,
            player: Some(player.clone()),
        }
    }
    pub fn with_option(pid: i8, player: Option<Player>) -> JoinedCell {
        JoinedCell {
            player_id: pid,
            player: player,
        }
    }
    pub fn from_id(pid: i8) -> JoinedCell {
        JoinedCell {
            player_id: pid,
            player: None,
        }
    }
    /// get the id of the Player
    pub fn id(&self) -> i8 {
        self.player_id
    }
    /// get the State the Player has when reaching this Position
    pub fn position(&self) -> Option<Player> {
        self.player.clone()
    }
}
impl Default for JoinedCell {
    fn default() -> JoinedCell {
        JoinedCell {
            player_id: -2,
            player: None,
        }
    }
}

#[derive(Debug)]
pub struct JoinedMap {
    cells: Vec<Vec<JoinedCell>>,
}

impl JoinedMap {
    fn empty(x: usize, y: usize) -> JoinedMap {
        JoinedMap {
            cells: (0..y).map(|_| {
                (0..x).map(|_| JoinedCell::default()).collect()
            }).collect()
        }
    }
}

impl Index<(usize, usize)> for JoinedMap {
    type Output = JoinedCell;
    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        &self.cells[y][x]
    }
}

impl IndexMut<(usize, usize)> for JoinedMap {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        &mut self.cells[y][x]
    }
}

