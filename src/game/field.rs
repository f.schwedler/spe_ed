
use super::{
    Player,
    Action,
};
use std::{
    collections::HashMap,
    ops::Index,
    cmp::{
        max,
        min,
    },
};

use chrono::{
    prelude::DateTime,
    offset::Utc,
    Duration,
};

use serde::{
    Serialize,
    Deserialize,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Field {
    width: usize,
    height: usize,
    cells: Vec<Vec<i8>>,
    players: HashMap<i8, Player>,
    you: i8,
    running: bool,
    #[serde(default = "Utc::now")]
    deadline: DateTime<Utc>,
}

impl Field {
    pub fn players(&self) -> &HashMap<i8, Player> {
        &self.players
    }
    pub fn you(&self) -> &Player {
        match self.players.get(&self.you) {
            Some(p) => &p,
            None => panic!("you dont exist!"),
        }
    }
    pub fn your_id(&self) -> i8 {
        self.you
    }
    pub fn running(&self) -> bool {
        self.running
    }
    pub fn deadline(&self) -> DateTime<Utc> {
        self.deadline
    }
    pub fn width(&self) -> usize {
        self.width
    }
    pub fn height(&self) -> usize {
        self.height
    }
    pub fn set_player_turns(&mut self, player: &HashMap<i8, Player>) -> HashMap<i8, Player> {
        for (i, p) in &mut self.players {
            match player.get(i) {
                Some(plyr) => {
                    if plyr.speed() ==  p.speed() {
                        p.set_turn(plyr.turn_number() + 1);
                    }
                }
                None => continue,
            }
        }
        self.players.clone()
    }
    /// get all new Positions for a Player by its leagal turns
    /// all stay inside the Field
    pub fn next_positions_by_player(&self, player: &Player) -> Vec<Player> {
        let mut pos = player.next_positions();
        pos.retain(|p| p.x() < self.width && p.y() < self.height());
        'positions: for p in &mut pos {
            if 0 != self[p.pos()] {
                p.kill();
                continue 'positions;
            }
            for f in p.skipped_fields() {
                if 0 != self[f] {
                    p.kill();
                    continue 'positions;
                }
            }
        }
        pos
    }
    /// like `next_positions_by_player` but with player id
    pub fn next_positions_by_id(&self, id: &i8) -> Vec<Player> {
        match self.players.get(id) {
            Some(player) => self.next_positions_by_player(player),
            None => Vec::new(),
        }
    }
    /// calls `next_positions_by_player` for all Player
    pub fn next_postions(&self) -> HashMap<i8, Vec<Player>> {
        let mut pos_map: HashMap<i8, Vec<Player>> = HashMap::new();
        for (i, p) in self.players.clone() {
            pos_map.insert(i, self.next_positions_by_player(&p));
        }
        pos_map
    }
    /// recursively check all posible moves for the player
    /// and check if it would survive,
    /// only checks already existing traces
    pub fn recursive_positions_by_player(&self, player: &Player, max_depth: usize) -> Vec<(Player, usize, usize)> {
        let mut players = Vec::new();
        for p in self.next_positions_by_player(player) {
            if p.active() {
                if 0 == max_depth
                || self.deadline.signed_duration_since(Utc::now()) < Duration:: microseconds(100) {
                    players.push((p, 1, 1));
                } else {
                    let v = self.recursive_positions_by_player(&p, max_depth - 1);
                    if 0 < v.len() {
                        // n := the number of posibilities
                        // d := the number of round survivable
                        let (n, d) = v.iter()
                            .fold((0, 0), |acc, x| (acc.0 + x.1, max(acc.1, x.2)));
                        players.push((p, n, d + 1));
                    }
                }
            } else {
                    players.push((p, 0, 0));
            }
        }
        players
    }
    /// Check if the rectangel given by the coordinates is empty
    pub fn check_empty_area(&self, (xstart, ystart): (usize, usize), (xend, yend): (usize, usize)) -> bool {
        let xmax = max(xstart, xend) + 1;
        let xmin = min(xstart, xend);
        let ymax = max(ystart, yend) + 1;
        let ymin = min(ystart, yend);
        if xmax > self.width() || ymax > self.height() {
            return false;
        }
        for y in ymin..ymax {
            for x in xmin..xmax {
                if 0 != self[(x, y)] {
                    return false;
                }
            }
        }
        true
    }
    /// get all actions that dont lead to death for player by id
    pub fn undead_actions_for_player_id(&self, pid: i8) -> Vec<Action> {
        match self.players.get(&pid) {
            Some(player) => self.undead_actions_for_player(player),
            None => Vec::new(),
        }
    }
    /// get all actions that dont lead to death for player
    pub fn undead_actions_for_player(&self, player: &Player) -> Vec<Action> {
        let speed = player.speed() as usize;
        /* (-s, -s) - (-1, -s)       (1, -s) - (s, -s)
         *    |          |               |        |
         *  (-s, 0) - (-1,  0)        (1,  0) - (s,  0)
         *    |          |     (0, 1)    |        |
         *    |          |       |       |        |
         *  (-s, s) - (-1,  s) (0, s) (1,  s) - (s,  s)
         */
        let pos = player.pos();
        let s = speed as i32;
        let area_bool = |pos, vec1, vec2| -> bool {
            match player.direction().add_vector_to_position(pos, vec1) {
                Some(e1) => match player.direction().add_vector_to_position(pos, vec2) {
                    Some(e2) => self.check_empty_area(e1, e2),
                    None => false,
                }
                None => false,
            }
        };
        let right_back = area_bool(pos, (-s, -s), (-1, 0));
        let left_back = area_bool(pos, (1, -s), (s, 0));

        let center_front = area_bool(pos, (0, 1), (0, s));
        let right_front = area_bool(pos, (-s, 0), (-1, s));
        let left_front = area_bool(pos, (1, 0), (s, s));

        let center_slow = area_bool(pos, (0, 1), (0, s - 1));
        let right_slow = area_bool(pos, (-(s - 1), 0), (-1, (s - 1)));
        let left_slow = area_bool(pos, (1, 0), (s - 1, s - 1));

        let center_fast = area_bool(pos, (0, 1), (0, s + 1));
        let right_fast = area_bool(pos, (-(s + 1), 0), (-1, s + 1));
        let left_fast = area_bool(pos, (1, 0), (s + 1, s + 1));

        let mut actions = Vec::new();
        for a in player.legal_actions() {
            match a {
                Action::ChangeNothing => {
                    if center_front && (!right_front || !left_front) {
                        actions.push(a);
                    }
                }
                Action::TurnRight => {
                    if right_back || right_front{
                        actions.push(a);
                    }
                }
                Action::TurnLeft => {
                    if left_back || left_front {
                        actions.push(a);
                    }
                }
                Action::SlowDown => {
                    if center_slow && (!right_slow || !left_slow) {
                        actions.push(a);
                    }
                }
                Action::SpeedUp => {
                    if center_fast && (!right_fast || !left_fast) {
                        actions.push(a);
                    }
                }
            }
        }
        actions
    }
}

impl Index<(usize, usize)> for Field {
    type Output = i8;
    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        &self.cells[y][x]
    }
}
