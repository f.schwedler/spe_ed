
use std::fmt;

use serde::{
    Serialize,
    Deserialize,
};

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq, Copy, Ord, PartialOrd)]
#[serde(rename_all = "snake_case")]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}
impl Direction {
    pub fn add_vector_to_position(&self, (xpos, ypos): (usize, usize), (xvec, yvec): (i32, i32)) -> Option<(usize, usize)> {
        let mut new_x = xpos as i32;
        let mut new_y = ypos as i32;
        match self {
            Direction::Down => {
                new_x += xvec;
                new_y += yvec;
            }
            Direction::Up => {
                new_x -= xvec;
                new_y -= yvec;
            }
            Direction::Left => {
                new_x -= yvec;
                new_y += xvec
            }
            Direction::Right => {
                new_x += yvec;
                new_y -= xvec;
            }
        }
        if 0 <= new_x && 0 <= new_y {
            Some((new_x as usize,  new_y as usize))
        } else {
            None
        }
    }
    pub fn positions_to_vector(&self, (x1, y1): (usize, usize), (x2, y2): (usize, usize)) -> (i32, i32) {
        let xvec = x2 as i32 - x1 as i32;
        let yvec = y2 as i32 - y1 as i32;
        match self {
            Direction::Down => (xvec, yvec),
            Direction::Up => (-xvec, -yvec),
            Direction::Left => (yvec,-xvec),
            Direction::Right => (-yvec, xvec),
        }
    }
}
// left", "right", "up" oder "down
impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let res = match self {
            Direction::Up => "🠝",
            Direction::Down => "🠟",
            Direction::Left => "🠜",
            Direction::Right => "🠞",
        };
        write!(f, "{}", res)
    }
}

impl Default for Direction {
    fn default() -> Self {
        Direction::Up
    }
}
