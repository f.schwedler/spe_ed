
use super::{
    Direction,
    Action,
};
use std::fmt;
use serde::{
    Serialize,
    Deserialize,
    de,
};

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Player {
    #[serde(deserialize_with = "usize_from_str")]
    x: usize,
    #[serde(deserialize_with = "usize_from_str")]
    y: usize,
    direction: Direction,
    speed: u8,
    #[serde(default = "u8::default")]
    turn: u8,
    active: bool,
    name: Option<String>,
}

impl Player {
    pub fn x(&self) -> usize {
        self.x
    }
    pub fn y(&self) -> usize {
        self.y
    }
    pub fn pos(&self) -> (usize, usize) {
        (self.x, self.y)
    }
    pub fn direction(&self) -> Direction {
        self.direction.clone()
    }
    pub fn speed(&self) -> u8 {
        self.speed
    }
    /// get the turn to know when jumping is posible
    pub fn turn_number(&self) -> u8 {
        self.turn
    }
    /// set the turn for jumps, jumps in Round 0 (while beein reset to 0)
    pub fn set_turn(&mut self, turn_number: u8) {
        // TODO check if jump round is corect
        self.turn = turn_number % 10;
    }
    pub fn active(&self) -> bool {
        self.active
    }
    pub fn kill(&mut self) {
        self.active = false;
    }
    pub fn name(&self) -> Option<String> {
        self.name.clone()
    }
    /// get a list of all actions the player can legaly take
    pub fn legal_actions(&self) -> Vec<Action> {
        if self.active {
            let mut res = vec![
                Action::ChangeNothing,
                Action::TurnLeft,
                Action::TurnRight,
            ];
            if self.speed < 10 {
                res.push(Action::SpeedUp);
            }
            if self.speed > 1 {
                res.push(Action::SlowDown);
            }
            res
        } else {
            Vec::new()
        }
    }
    /// Change Speed and Direction acording to the given Action
    /// if the Speed would go above 10 or below 0 the Player is set to be no longer active instead
    fn update_action(&mut self, action: Action) {
        match action {
            Action::SlowDown => {
                if 0 == self.speed() {
                    self.active = false;
                } else {
                    self.speed -= 1;
                }
            },
            Action::SpeedUp => {
                if 10 == self.speed() {
                    self.active = false;
                } else {
                    self.speed += 1;
                }
            },
            Action::TurnLeft => self.direction = match self.direction() {
                Direction::Up => Direction::Left,
                Direction::Left => Direction::Down,
                Direction::Down => Direction::Right,
                Direction::Right => Direction::Up,
            },
            Action::TurnRight => self.direction = match self.direction() {
                Direction::Up => Direction::Right,
                Direction::Right => Direction::Down,
                Direction::Down => Direction::Left,
                Direction::Left => Direction::Up,
            },
            Action::ChangeNothing => {},
        }
    }
    /// Change the Position acording to the current Speed and Direction
    /// if a Coordinate whould get negative the Player is set to be no longer active instead
    fn take_turn(&mut self) {
        if !self.active() {
            return
        }
        let s = self.speed as usize;
        self.turn = (self.turn + 1) % 10;
        match self.direction() {
            Direction::Up => {
                if self.y >= s {
                    self.y -= s;
                } else {
                    self.active = false;
                }
            }
            Direction::Down => {
                self.y += s;
            }
            Direction::Left => {
                if self.x >= s {
                    self.x -= s;
                } else {
                    self.active = false;
                }
            }
            Direction::Right => {
                self.x += s;
            }
        }
    }
    /// Calculates all new Positions based on the legal turns
    /// Positions Where a Coordinate would get negative are removed
    /// Positions with Coordinates higher than the field width/height are still there
    pub fn next_positions(&self) -> Vec<Player> {
        let mut pos = Vec::new();
        for action in self.legal_actions() {
            let mut tmp = self.clone();
            tmp.update_action(action);
            tmp.take_turn();
            if tmp.active() {
                pos.push(tmp);
            }
        }
        pos
    }
    pub fn skipped_fields(&self) -> Vec<(usize, usize)> {
        let speed = self.speed as usize;
        let range = 1..speed;
        if 0 == self.turn {
            // jump if in the 10th turn
            if 1 < self.speed() {
                match self.direction {
                    Direction::Up => vec![(self.x, self.y + 1)],
                    Direction::Down => vec![(self.x, self.y - 1)],
                    Direction::Left => vec![(self.x + 1, self.y)],
                    Direction::Right => vec![(self.x - 1, self.y)],
                }
            } else {
                Vec::new()
            }
        } else {
            match self.direction {
                Direction::Up => range.map(|x| (self.x, self.y + x)).collect(),
                Direction::Down => range.map(|x| (self.x, self.y - x)).collect(),
                Direction::Left => range.map(|x| (self.x + x, self.y)).collect(),
                Direction::Right => range.map(|x| (self.x - x, self.y)).collect(),
            }
        }
    }
    pub fn action_from_position(&self, other: &Player) -> Option<Action> {
        match self.direction().positions_to_vector(self.pos(), other.pos()) {
            (0, y) if 0 < y => {
                if other.speed() == self.speed() {
                    Some(Action::ChangeNothing)
                } else if other.speed() == self.speed() + 1 {
                    Some(Action::SpeedUp)
                } else if other.speed() + 1 == self.speed() {
                    Some(Action::SlowDown)
                } else {
                    None
                }
            },
            (x, 0) if self.speed() as i32 == x => Some(Action::TurnLeft),
            (x, 0) if -(self.speed() as i32) == x => Some(Action::TurnRight),
            _ => None,
        }
    }
}

fn usize_from_str<'de, D>(deserializer: D) -> Result<usize, D::Error>
where D: de::Deserializer<'de> {
    struct JsonUsizeVisitor;

    impl<'de> de::Visitor<'de> for JsonUsizeVisitor {
        type Value = usize;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a sting containing a number")
        }
        fn visit_i64<E>(self, i: i64) -> Result<Self::Value, E>
            where E: de::Error {
                if i < 0 {
                    Ok(0)
                } else {
                    Ok(i as usize)
                }
            }
        fn visit_u64<E>(self, i: u64) -> Result<Self::Value, E>
            where E: de::Error {
                Ok(i as usize)
        }
    }
    deserializer.deserialize_u64(JsonUsizeVisitor)
}
