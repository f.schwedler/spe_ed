
use serde::{
    Serialize,
    Deserialize,
};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum Action {
    ChangeNothing,
    TurnLeft,
    TurnRight,
    SpeedUp,
    SlowDown,
}

impl Default for Action {
    fn default() -> Self {
        Action::ChangeNothing
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
//#[serde(rename_all = "snake_case")]
pub struct Reply {
    pub action: Action,
}

impl Reply {
    pub fn new(action: Action) -> Reply {
        Reply { action: action }
    }
}
